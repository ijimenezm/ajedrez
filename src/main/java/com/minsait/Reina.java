package com.minsait;

import java.util.List;

public class Reina extends Pieza {
    private Casilla casilla;

    public void setCasilla(Casilla casilla) {
        this.casilla = casilla;
        casilla.setEstado(Estado.OCUPADO);
        casilla.setPieza(new Reina());
    }

    void llenarCasillasAtacadas(Tablero tablero) {
        Casilla casillaEncontrada;

        for (int indiceRenglon = 0; indiceRenglon < tablero.getTamanio(); indiceRenglon++) {
            for (int indiceColumna = 0; indiceColumna < tablero.getTamanio(); indiceColumna++) {
                int difIndicesColumna = indiceColumna - casilla.getPosicionColumna();
                int difIndicesRenglon = indiceRenglon - casilla.getPosicionFila();
                if (difIndicesColumna == difIndicesRenglon || difIndicesColumna == - difIndicesRenglon
                        || difIndicesColumna == 0 || difIndicesRenglon == 0) {
                    casillaEncontrada = tablero.getCasillas().get(indiceRenglon).get(indiceColumna);
                    colocarAtaque(casillaEncontrada);
                }
            }
        }

    }

    private void colocarAtaque(Casilla casillaEncontrada) {

        if (casillaEncontrada.getEstado() == Estado.VACIO) {
            casillaEncontrada.setEstado(Estado.ATACADO);
        }
    }

    void movimientoFilaInterminable(List<Casilla> casillasFilaInterminable) {
        //TODO: Generar el movimiento de la reina
    }

    public static void main(String[] args) {
        Tablero tablero = new Tablero();
        Reina reina = new Reina();
        reina.llenarCasillasAtacadas(tablero);
        //tablero.getCasillas().get(0).get(0).setPieza(reina);
        tablero.imprimirTablero();
    }
}
