package com.minsait;

import java.util.ArrayList;

public class Simulador {
    private ArrayList<Tablero> soluciones;

    public Simulador(){
        soluciones = new ArrayList<Tablero>();
    }

    public void colocarReinas(int fila, Tablero tablero){
        for (int columna = 0; columna < tablero.getTamanio(); columna++) {
            Tablero tablero1 = new Tablero(tablero.getCasillas());
            Casilla casillaActual = tablero1.getCasillas().get(fila).get(columna);
            if (casillaActual.getEstado() == Estado.VACIO) {
                Reina reina = new Reina();
                reina.setCasilla(casillaActual);
                casillaActual.setPieza(reina);
                casillaActual.setEstado(Estado.OCUPADO);
                reina.llenarCasillasAtacadas(tablero1);
                if (fila + 1 < 8) {
                    colocarReinas(fila + 1, tablero1);
                }
                else {
                    soluciones.add(tablero1);
                }
            }
        }
    }

    public ArrayList<Tablero> iniciarPruebas(){
        Tablero tablero = new Tablero();
        colocarReinas(0,tablero);
        return  soluciones;
    }

    public static void main(String[] args) {
        Simulador simulador = new Simulador();
        ArrayList<Tablero> soluciones = simulador.iniciarPruebas();
        for (Tablero tablero : soluciones) {
            tablero.imprimirTablero();
            System.out.println("\n\n\n");
        }
        System.out.println(soluciones.size());
    }
}
