package com.minsait;

public class Casilla {

    private boolean color;
    private Integer posicionColumna;
    private Integer posicionFila;
    private Estado estado;
    private Pieza pieza;

    public Pieza getPieza() {return pieza;}

    public Casilla(boolean color, Integer posicionColumna, Integer posicionFila) {
        this.color = color;
        this.posicionColumna = posicionColumna;
        this.posicionFila = posicionFila;
        this.estado = Estado.VACIO;
    }

    public Casilla(boolean color, Integer posicionColumna, Integer posicionFila,Estado estado, Pieza pieza) {
        this.color = color;
        this.posicionColumna = posicionColumna;
        this.posicionFila = posicionFila;
        this.estado = estado;
        this.pieza = pieza;
    }

    public boolean getColor() {
        return color;
    }

    public Integer getPosicionColumna() {
        return posicionColumna;
    }

    public Integer getPosicionFila() {
        return posicionFila;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public void setPieza(Pieza pieza) {
        this.pieza = pieza;
    }

    @Override
    public String toString() {
        return "Casilla{" +
                "color=" + color +
                ", posicionColumna=" + posicionColumna +
                ", posicionFila=" + posicionFila +
                ", estado=" + estado +
                ", pieza=" + pieza +
                "\n}";
    }
}
